/*
 * Copyright (c) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.navigationdrawerexample;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.navigationdrawerexample.listeners.FloatingDrawerClickListener;


/*
 * VerticalGridActivity that loads SmallDrawerItemFragment
 */
public class VerticalGridActivity extends FragmentActivity implements  View.OnFocusChangeListener, FloatingDrawerClickListener {


    private FrameLayout fixed_drawer,floating_drawer;



    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_grid);

        fixed_drawer = findViewById(R.id.fixed_drawer);
        floating_drawer = findViewById(R.id.floating_drawer);
        //getWindow().setBackgroundDrawableResource(R.id.browse_grid);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

/*        if (hasFocus && !AppConstants.DRAWER_OPEN) {
            Utils.animateView(floating_drawer, AppConstants.ANIMATION_RIGHT);
            AppConstants.DRAWER_OPEN = true;
        } else if (!hasFocus && AppConstants.DRAWER_OPEN) {
            Utils.animateView(floating_drawer, AppConstants.ANIMATION_LEFT);
            AppConstants.DRAWER_OPEN = false;
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppConstants.DRAWER_OPEN)
                    fixed_drawer.setAlpha(0.0F);
                else
                    fixed_drawer.setAlpha(1.0F);
            }
        },480);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppConstants.DRAWER_OPEN)
                    fixed_drawer.setAlpha(0.0F);
                else
                    fixed_drawer.setAlpha(1.0F);
            }
        },50);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                homeActivityBinding.focusButton.setSelection(selectedPostion)
            }
        },50);*/

    }

    @Override
    public void onItemSelected(boolean hasFocus) {

        if(AppConstants.DRAWER_OPEN = false){
            Utils.animateView(floating_drawer, AppConstants.ANIMATION_LEFT);
            AppConstants.DRAWER_OPEN = true;
        }
        else if(AppConstants.DRAWER_OPEN =true) {
            Utils.animateView(floating_drawer, AppConstants.ANIMATION_RIGHT);
            AppConstants.DRAWER_OPEN = false;
        }



//        if (hasFocus && AppConstants.DRAWER_OPEN) {
//            Utils.animateView(floating_drawer, AppConstants.ANIMATION_LEFT);
//            AppConstants.DRAWER_OPEN = false;
//        } else if (!hasFocus && AppConstants.DRAWER_OPEN) {
//            Utils.animateView(floating_drawer, AppConstants.ANIMATION_RIGHT);
//            AppConstants.DRAWER_OPEN = true;
//        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppConstants.DRAWER_OPEN)
                    fixed_drawer.setAlpha(0.0F);
                else
                    fixed_drawer.setAlpha(1.0F);
            }
        },480);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppConstants.DRAWER_OPEN)
                    fixed_drawer.setAlpha(0.0F);
                else
                    fixed_drawer.setAlpha(1.0F);
            }
        },50);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                homeActivityBinding.focusButton.setSelection(selectedPostion)
            }
        },50);
    }

    @Override
    public void onItemClicked() {
        Toast.makeText(this, "onItemClicked", Toast.LENGTH_SHORT).show();
    }
}
