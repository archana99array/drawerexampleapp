package com.example.navigationdrawerexample;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class Utils {

    public static void animateView(View view, int direction) {
        Animation swipeAnimation = null;
        switch (direction) {

            case AppConstants.ANIMATION_LEFT :
                    swipeAnimation =  AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_left);
                break;


            case AppConstants.ANIMATION_RIGHT :
                swipeAnimation =  AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_right);
                break;
        }
        swipeAnimation.setFillAfter(true);
        view.startAnimation(swipeAnimation);
    }
}
