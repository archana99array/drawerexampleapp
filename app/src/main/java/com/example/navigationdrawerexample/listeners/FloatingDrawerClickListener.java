package com.example.navigationdrawerexample.listeners;

public interface FloatingDrawerClickListener {
    void onItemSelected(boolean hasFocus);
    void onItemClicked();
}
