package com.example.navigationdrawerexample;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v17.leanback.app.VerticalGridFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v17.leanback.widget.VerticalGridPresenter;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class FloatingDrawerItemFragment extends VerticalGridFragment {

    private static final String TAG = "leanback.SmallDrawerItemFragment";
    private static final int NUM_COLUMNS = 1;
    private static final int NUM_ITEMS = 50;
    private static final int HEIGHT = 100;
    private ArrayObjectAdapter mAdapter;
    private Random mRandom;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mRandom = new Random();
        //params = new Params();

                /*Params p = new Params();
                p.setBadgeImage(getActivity().getResources().getDrawable(R.drawable.ic_title));
                p.setTitle("Leanback Vertical Grid Demo");
                setParams(p);*/
        setTitle("Vertical Grid Fargment");
        setupFragment();
    }

    private void setupFragment() {
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

        mAdapter = new ArrayObjectAdapter(new GridItemPresenter());
        for (int i = 0; i < 10; i++) {
            mAdapter.add(new MyItem(i));
        }
        setAdapter(mAdapter);
        setOnItemViewSelectedListener(new OnItemViewSelectedListener() {
            @Override
            public void onItemSelected(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder1, Row row) {
                Toast.makeText(getActivity(), "Item Selected", Toast.LENGTH_SHORT).show();
            }
        });

        setOnItemViewClickedListener(new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder1, Row row) {
                Toast.makeText(getActivity(), "Item Clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private class GridItemPresenter extends Presenter {
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            TextView view = new TextView(parent.getContext());
            // Choose a random height between HEIGHT and 1.5 * HEIGHT to
            // demonstrate the staggered nature of the grid.
            final int height = HEIGHT + mRandom.nextInt(HEIGHT / 2);
            view.setLayoutParams(new ViewGroup.LayoutParams(200, height));
            view.setFocusable(true);
            view.setFocusableInTouchMode(true);
            view.setBackgroundColor(Color.DKGRAY);
            view.setGravity(Gravity.START);
            return new ViewHolder(view);
        }

        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            ((TextView) viewHolder.view).setText(Integer.toString(((MyItem) item).id));
        }

        public void onUnbindViewHolder(ViewHolder viewHolder) {
        }
    }

    static class MyItem {
        int id;

        MyItem(int id) {
            this.id = id;
        }
    }
}
